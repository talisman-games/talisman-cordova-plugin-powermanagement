# talisman-cordova-plugin-powermanagement

This repo is a fork of https://github.com/Viras-/cordova-plugin-powermanagement, intended for talisman.games internal use.

## Installation

Install the plugin using the cordova command line:

```bash
cordova plugin add talisman-cordova-plugin-powermanagement
```

## Usage

See https://github.com/Viras-/cordova-plugin-powermanagement for API documentation.

## License

This plugin has been forked and modified from the works of Wolfgang Koller in 2013.

Licensed under the [Apache 2.0 License](./LICENSE).
